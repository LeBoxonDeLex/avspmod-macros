# Generate an *.avs file from a video.

import wx
import os
import re


def addOrReplaceLogo( logoLoc, x, y ):
	
	logoDecl = 'logo = ImageSource("' + logoLoc + '", pixel_type="RGB32")'
	txt = avsp.GetText()
	if not txt.contains( logoDecl ):
		txt += '\n' + logoDecl + '\n'
	
	m = re.search( r'(overlay.*)', txt, re.M | re.I )
	if m:
		overlayDecl = 'overlay( logo, ' + x + ', ' + y + ', mask=logo.showalpha(), mode ="blend" )'
		txt = txt.replace( m.group( 1 ), overlayDecl )
		
	avsp.SetText( txt )


def findLogoLocation():

	loc = avsp.Options['lex_logo']
	if not os.path.exists( loc ):
		options = wx.OPEN|wx.FILE_MUST_EXIST
		dlg = wx.FileDialog(avsp.GetWindow(), 'Image', dir, '', '*.png', options)
	
		ID = dlg.ShowModal()
		if ID == wx.ID_OK:
			loc = dlg.GetPaths()[ 0 ]
			avsp.Options['lex_logo'] = loc
		
		dlg.Destroy()
	
	return loc


def findLogoPosition():

	global logoX
	global logoY
	
	txt = avsp.GetText()
	m = re.search( r'overlay.*,\s*(\d+)\s*,\s*(\d+).*', txt, re.M | re.I )
	if m:
		logoX = m.group(1)
		logoY = m.group(2)
	else:
		avsp.MsgBox("Pensez � corriger l'offset !!!")


def createLogoDialog(parent):
	
	global logoX
	global logoY
	
	options = wx.DEFAULT_DIALOG_STYLE|wx.STAY_ON_TOP
	dlg = wx.Dialog(parent, wx.ID_ANY, 'Logo', style=options)
	sizer = wx.BoxSizer(wx.VERTICAL)
		
	# Text
	staticText = wx.StaticText(dlg, wx.ID_ANY, 'Positionnez le logo.' )
	sizer.Add(staticText, 0, wx.ALL, 10)
	
	# Create the spinners
	spinSizer = wx.GridBagSizer(hgap=4, vgap=4)
	sizer.Add(spinSizer, 1, wx.ALL, 10)
	spinInfo = (
	    ('x', (0,0), (0,1), logoX),
	    ('y', (1,0), (1,1), logoY)
	)
		
	for name, txtPos, spinPos, value in spinInfo:
	    staticText = wx.StaticText(dlg, wx.ID_ANY, name)
	    spinCtrl = wx.SpinCtrl(dlg, wx.ID_ANY, value, size=(60,-1))
	    #spinCtrl.Bind(wx.EVT_TEXT, self.OnCropDialogSpinTextChange)
	    spinSizer.Add(staticText, txtPos, flag=wx.ALIGN_CENTER|wx.RIGHT, border=5)
	    spinSizer.Add(spinCtrl, spinPos, flag=wx.EXPAND|wx.RIGHT, border=0)
		
	# Create the dialog buttons
	buttonApply = wx.Button(dlg, wx.ID_OK, _('Apply'))
	#dlg.Bind(wx.EVT_BUTTON, self.OnCropDialogApply, buttonApply)
	buttonCancel = wx.Button(dlg, wx.ID_CANCEL, _('Cancel'))
	#dlg.Bind(wx.EVT_BUTTON, self.OnCropDialogCancel, buttonCancel)
	buttonSizer = wx.BoxSizer(wx.HORIZONTAL)
	buttonSizer.Add(buttonApply, 0, wx.ALL, 5)
	buttonSizer.Add(buttonCancel, 0, wx.ALL, 5)
		
	sizer.Add(wx.StaticLine(dlg), 0, wx.EXPAND)
	sizer.Add(buttonSizer, 0, wx.ALIGN_CENTER|wx.ALL, 10)
		
	# Complete the dialog
	dlg.SetSizer(sizer)
	dlg.Fit()
		
	# Events
	#dlg.Bind(wx.EVT_CLOSE, self.OnCropDialogCancel)
	buttonApply.SetDefault()
	return dlg
	

def selectLogoPosition():
	
	dlg = createLogoDialog(avsp.GetWindow())
	#LogoPositionDialog()
	
	ID = dlg.ShowModal()
	dlg.Destroy()

	
	
# Default action

logoX = '0'
logoY = '0'
findLogoPosition()

selectLogoPosition()
