# Selects VideoRedo project files and generates AVS
# files from a given skeleton.

import sys, locale, os, wx, re

from os import listdir
from os.path import isfile, join
from avsp import AsyncCallWrapper, AsyncCall

#
# Use avsp.SetText( msg ) to debug.
#

# Deal with character encoding
reload(sys)
sys.setdefaultencoding(locale.getdefaultlocale()[1])

# Global "preference"
AVSP_MOD_LOC = os.getcwd() + '\\macros\\lex-templates'


# Find Vprj files
@AsyncCallWrapper
def getVprjFiles():

	dir = ''
	options = wx.OPEN|wx.FILE_MUST_EXIST|wx.FD_MULTIPLE
	dlg = wx.FileDialog(avsp.GetWindow(), 'VideoRedo Projects', dir, '', '*.VPrj', options)
	
	filenames = []
	if dlg.ShowModal() == wx.ID_OK:
		filenames = dlg.GetPaths()
		
	dlg.Destroy()
	return filenames


# Get the FPS
@AsyncCallWrapper
def getFps():
	
	fps = '25'
	options = wx.OK | wx.CANCEL
	dlg = wx.TextEntryDialog( avsp.GetWindow(), 'FPS', 'Quel FPS utiliser ?', fps, options )
	
	if dlg.ShowModal() == wx.ID_OK:
		fps = dlg.GetValue()
	else:
		fps = ''
	
	dlg.Destroy()
	return fps


# Get the template to use
@AsyncCallWrapper
def getTemplate():

	templateFiles = [f for f in listdir(AVSP_MOD_LOC) if isfile(join(AVSP_MOD_LOC, f))]
	dlg = wx.SingleChoiceDialog( avsp.GetWindow(), 'Template', 'Quel template utiliser ?', templateFiles )
	
	templateName = '';
	if dlg.ShowModal() == wx.ID_OK:
		templateName = templateFiles[ dlg.GetSelection()]

	dlg.Destroy()
	return templateName


# Find the cuts list.
# Copied from ImportVideoRedoCuts but modified
def getCutList( vprjFileContent, fps ):
	
	ltrimlist = vprjFileContent.split("CutStart=\"")
	rtrimlist = vprjFileContent.split("CutEnd=\"")
	duration = re.search( r'<Duration>(.*)</Duration>', vprjFileContent, re.M | re.I )
	if not duration:
		return '# Oops! Could not find duration in VRD project.'
	
	trimlist = ''
	if (len(ltrimlist) == len(rtrimlist)) and len(ltrimlist) > 1:
		
		# No cut at the beginning?
		if (ltrimlist[1][:11] != "00:00:00;00"):
			frameEnd = convToFrameNb(fps, ltrimlist[1][:11])
			frameEnd -= 1 # Lex: remove 1 frame
			trimlist += "0," + str(frameEnd)
			if len(ltrimlist) > 2:
				trimlist += ";" #s�parateur

		# Middle cuts
		for n in range(2, len(ltrimlist)):
			frameStart = convToFrameNb(fps, rtrimlist[n-1][:11])
			frameEnd = convToFrameNb(fps, ltrimlist[n][:11])
			frameEnd -= 1 # Lex: remove 1 frame
			trimlist += str(frameStart) + "," + str(frameEnd)
			if n < len(ltrimlist) - 1:
				trimlist += ";" #s�parateur

		# No cut at the end?
		lastIndex = len(rtrimlist) - 1
		frameStart = convToFrameNb(fps, rtrimlist[lastIndex][:11])
		
		# Duration contains the time in seconds, followed by 7 digits
		durationInSeconds = duration.group(1)[:-7]
		maxFrameEnd = int(durationInSeconds) * fps
		
		# If there is no significant difference between the values,
		# then there is a scene at the end
		if( maxFrameEnd - frameStart > 30 ):
			if trimlist:
				trimlist += ';'
			
			# VRD's FPS is not always precise, cut 2 frames for safety
			trimlist += str(frameStart) + ',last.frameCount() - 2'
		
		return trimlist

	return ''
# end def #############################


# convToFrameNb (time converted into frame number)
# Copied from ImportVideoRedoCuts but modified
def convToFrameNb(fps, h):
	datelist = h.split(":")
	h = int(datelist[0])
	m = int(datelist[1])
	s = int(datelist[2].split(";")[0])
	f = int(datelist[2].split(";")[1])
	return int((h * 3600 + m * 60 + s) * fps + f)
	
	
# Generate AVS files
def generateAvsFiles(filenames, fps, templateName):
	
	# The result is an array of generated AVS files
	generated = []
	
	# Read the template
	avsTplFile = open( AVSP_MOD_LOC + '\\' + templateName, 'r' )
	avsTpl = avsTplFile.read()
	avsTplFile.close()
	
	# Instantiate it for every VPrj file
	for f in filenames:
		projFile = open( f, 'r' )
		projFileContent = projFile.read()
		projFile.close()
		
		err = re.search( r'<Filename>.*\\([^\\]+)</Filename>', projFileContent, re.M | re.I )
		if not err:
			avsp.SetText( 'Oh! No!' )
			return generated
		
		# Update the file name
		fixedFileName = err.group( 1 )
		fixedFileName = fixedFileName.replace( '&apos;', "'" )
		fixedFileName = fixedFileName.replace( '&amp;', '&' )
		fixedFileName = fixedFileName.replace( '&eacute;', '�' )
		fixedFileName = fixedFileName.replace( '&egrave;', '�' )
		fixedFileName = fixedFileName.replace( '&ccedil;', '�' )
		fixedFileName = fixedFileName.replace( '&agrave;', '�' )
		
		# Basic stuff
		contentToWrite = avsTpl
		contentToWrite = contentToWrite.replace( '__PLUGINSPATH__', AVSP_MOD_LOC + '\\plugins' )
		contentToWrite = contentToWrite.replace( '__FPS__', fps )
		contentToWrite = contentToWrite.replace( '__VID__', fixedFileName)
		
		# Import VideoRedo cuts (and remove one frame per trim)
		cutlist = getCutList( projFileContent, int( fps ))
		trimList = ''
		if cutlist:
			trimList += "## Trim d'ajustement\noffset=0"
			trimList += "\nTrim(offset, last.framecount)\n\n"
			trimList += "## Trim extraits depuis les coupes VideoRedo\n"
			trimList += "Trim(" + (") ++ Trim(").join(cutlist.split(";")) + ")\n"
		else:
			trimList = '## Trim'
		
		contentToWrite = contentToWrite.replace( '__TRIM__', trimList )
		
		# Write the file
		newFileName = f + '.avs'
		newFile = open( newFileName, 'w' );
		newFile.write( contentToWrite )
		newFile.close()
		
		generated.append( newFileName )

	return generated

###################
# Default behavior
###################

# Close the preview panel (for performance purpose)
AsyncCall(avsp.HideVideoWindow).Wait()

# Select the project files
filenames = getVprjFiles()
if len( filenames ) == 0:
	return

# Ask the FPS and template to use
fps = getFps()
if len( fps ) == 0:
	return

templateName = getTemplate()
if len( templateName ) == 0:
	return

# Generate
generated = generateAvsFiles(filenames, fps, templateName)

# Open the files
for path in generated:
	avsp.OpenFile( filename=path )

# Show the video frame
if generated:
	avsp.ShowVideoFrame()
