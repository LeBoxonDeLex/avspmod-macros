# Splits all the trims and create one file per trim.
# Each trim is considered as being a WEBM.

import os
import sys
import re

# Get the TRIMs
scriptlines = avsp.GetText().encode('utf-8').splitlines()
newscripttxt = ''
for l in scriptlines:
	
	# If we have trims, process them...
	if re.match( 'trim\s*\(\s*\d+.+', l, re.IGNORECASE ):

		splittrim = l.split('++')
		for i in range(len(splittrim)):
			contentToWrite = newscripttxt
			contentToWrite += '# ' + l + '\n\n# WEBMs\n\n'
			loopCpt = 0

			lengthDiff = -1
			for trim in splittrim:
				
				# Prepare the line output
				if i != loopCpt:
					contentToWrite += '# '

				trim = trim.strip()
				contentToWrite += 'sc = ' + trim + '\n'
				loopCpt += 1
				
				# Remember the length of the output scene
				if i+1 != loopCpt:
					continue
				
				strtrimlist = trim[5:len(trim)-1].split(',')
				if len(strtrimlist) == 2:
					frame1 = strtrimlist[0].strip()
					frame2 = strtrimlist[1].strip()
					if (not frame1.isdigit()) or (not frame2.isdigit()):
						continue

					lengthDiff = int(frame2) - int(frame1)

			# Complete the file's content
			if lengthDiff < 46:
				contentToWrite += '\nsc ++ reverse( sc )\n'
				contentToWrite += '# sc\n'
				contentToWrite += '# sc.assumeFPS( 15 ).changeFPS( 25 )\n'
			else:
				contentToWrite += '\nsc\n'
				contentToWrite += '# sc ++ reverse( sc )\n'
				contentToWrite += '# sc.assumeFPS( 15 ).changeFPS( 25 )\n'

			# Write the content
			path = avsp.GetScriptFilename()
			path = path.replace( '.avs', '_{:02d}.avs'.format(i+1))

			newFile = open( path, 'w' );
			newFile.write( contentToWrite )
			newFile.close()

		# ... and do not go further
		break
	
	# Otherwise, append the text
	newscripttxt += l + '\n'
