# Generate an *.avs file from a video.

import wx
import os
from avsp import AsyncCallWrapper


@AsyncCallWrapper
def getVideoFile():

	dir = ''
	options = wx.OPEN|wx.FILE_MUST_EXIST
	dlg = wx.FileDialog(avsp.GetWindow(), 'Vid�o', dir, '', '*.mkv;*.mp4', options)
	
	filePath = ''
	ID = dlg.ShowModal()
	if ID == wx.ID_OK:
		filePath = dlg.GetPaths()[ 0 ]
	
	dlg.Destroy()
	return filePath


def generateAvsFile( filePath ):
	
	contentToWrite = 'DirectShowSource("' + filePath + '")'
	contentToWrite += '\n'
	if filePath.endswith( '.ts '):
		contentToWrite += 'LanczosResize( 1024, 576 )'
		contentToWrite += '\n'
	
	newFileName = filePath + '.avs'
	newFile = open( newFileName, 'w' );
	newFile.write( contentToWrite )
	newFile.close()


# Default behavior
filePath = getVideoFile()
if filePath:
	generateAvsFile( filePath )
	avsp.OpenFile( filename=filePath )
	avsp.ShowVideoFrame()
