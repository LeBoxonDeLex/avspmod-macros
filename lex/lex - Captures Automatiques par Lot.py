# Generate an *.avs file from a video.

import wx
import os
import os.path
import re
import wx
import pyavs
from avsp import AsyncCallWrapper

@AsyncCallWrapper
def getVideoFiles():

	dir = ''
	options = wx.OPEN|wx.FILE_MUST_EXIST|wx.FD_MULTIPLE
	dlg = wx.FileDialog(avsp.GetWindow(), 'Vid�o', dir, '', '*.mkv', options)
	
	filePaths = []
	ID = dlg.ShowModal()
	if ID == wx.ID_OK:
		filePaths = dlg.GetPaths()
	
	dlg.Destroy()
	return filePaths


def updateTextContent( filePath ):
	
	contentToWrite = 'DirectShowSource("' + filePath + '")'
	contentToWrite += '\n'
	if filePath.endswith( '.ts '):
		contentToWrite += 'LanczosResize( 1024, 576 )'
		contentToWrite += '\n'
	
	avsp.SetText(contentToWrite)
	

global global_target_dir
global_target_dir = None
global_screenshot_padding = 1
	
def getOptions( filePath ):

	# run in thread
	self = avsp.GetWindow()
	target_dir = os.path.join( os.path.dirname(filePath), 'caps-a-trier')
	screenshot_padding = 20

	# Find the output directory
	while True:
		options = avsp.GetTextEntry(
			title='Options',
			message=['R�pertoire cible', 'Espacement entre les caps'],
            default=[target_dir, (screenshot_padding, 1, 200)],
            types=['file_save', 'spin'])

		if not options: return
		target_dir, screenshot_padding = options
		break
	
	if not os.path.exists( target_dir ):
		os.makedirs( target_dir )
	
	global global_target_dir
	global global_screenshot_padding
	global_target_dir = target_dir
	global_screenshot_padding = screenshot_padding
	
	return


@AsyncCallWrapper
def LexMacroProgressBox(title, max=100):

	options = wx.PD_CAN_ABORT|wx.PD_ELAPSED_TIME|wx.PD_REMAINING_TIME|wx.PD_AUTO_HIDE
	dlg = wx.ProgressDialog(parent=avsp.GetWindow(), title=title, message='', maximum=max, style=options )
	
	dlg.SetInitialSize((900, 200))
	return dlg


def createSnapshots( filePath ):
	# run in thread
	self = avsp.GetWindow()
	
	# Load the clip
	AVS = pyavs.AvsClip(avsp.GetText(clean=True), matrix=self.matrix, interlaced=self.interlaced, swapuv=self.swapuv)

	# Evaluate the script
	if AVS.IsErrorClip():
		avsp.MsgBox(AVS.error_message, 'Erreur')
		return False

	# Get list of frames
	global global_screenshot_padding
	frame_count = avsp.GetVideoFramecount()
	frames = range(0, frame_count, global_screenshot_padding)
	scene_digits = len(str(len(frames)))
	total_frames = len(frames)

	# Prepare filename template
	basename = os.path.basename(filePath)
	basename = re.sub(r"(?i)\.(ts|mkv|avs)$", '', basename )

	progress = LexMacroProgressBox(basename, total_frames)
	basename = os.path.join( global_target_dir, basename )

	# Save the images
	result = True
	for i, frame in enumerate(frames):
		filename = basename + '_' + str(frame).zfill(5) + '.jpg'
		disp =  str(i+1) + ' / ' + str(total_frames)
		if not avsp.SafeCall( progress.Update, i+1, disp)[0]:
			result = False
			break

		ret = self.SaveImage(
				filename,
				frame=frame,
				avs_clip=AVS,
				quality=95,
				depth=8)

		if not ret:
			result = False
			break

	avsp.SafeCall(progress.Destroy)
	return result


# Default behavior
filePaths = getVideoFiles()
if filePaths:
	getOptions( filePaths[ 0 ])
	if not global_target_dir is None and os.path.exists( global_target_dir ):
		for filePath in filePaths:
			updateTextContent( filePath )
			if not createSnapshots( filePath ):
				break
	
	# Reset the text in the editor
	avsp.SetText('')
	avsp.CloseTab()
