# Create a snapshot from a video.

import wx
import os
import re

def saveCapture():
	
	# Find the location of the script.
	path = avsp.GetScriptFilename()
	if path:
		path = path.replace( '.avs', '' )
	
	# If the script was not saved, find the video location in the script.
	else:
		m = re.search( r'DirectShowSource\("(.*)"\)', avsp.GetText(), re.M | re.I )
		if m:
			path = m.group(1)
		else:
			avsp.MsgBox( 'Oops ! Je ne sais pas o� sauvegarder la capture.' )
	
	# Remove the file extension and everything that was after the date, if any.
	imgBaseName = os.path.basename( path )
	imgBaseName = os.path.splitext( imgBaseName )[0]
	m = re.search( r'(.*_\d{2}_\d{2}_\d{2}).*', imgBaseName, re.M | re.I )
	if m:
		imgBaseName = m.group(1)
	
	# Create a directory to contain snapshots
	dir = os.path.join( os.path.dirname( path ), imgBaseName )
	if not os.path.exists( dir ):
		os.makedirs( dir )
	
	# Find the new ID and save it
	index = 1;
	output = os.path.join( dir, imgBaseName + '_' + str(index).zfill(2) + '.jpg' )
	while os.path.exists( output ):
		index += 1
		output = os.path.join( dir, imgBaseName + '_' + str(index).zfill(2) + '.jpg' )
	
	avsp.SaveImage( filename=output, quality=95 )


# Default action
saveCapture()
