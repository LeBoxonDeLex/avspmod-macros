# Register AVS files in MeGUI as MP4 jobs (webms).

import wx
import os
import re
from avsp import AsyncCallWrapper

global job_tpl
global job_list_tpl
global megui_path
global index

# #########################
#  Mettre � jour ce chemin
# #########################
megui_path = 'K:/Outils/MeGUI/jobs/'

# Misc
job_list_tpl = """<?xml version="1.0"?>
<JobListSerializer xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <mainJobList>
    {{ LIST }}
  </mainJobList>
  <workersAndTheirJobLists />
</JobListSerializer>
"""

job_tpl = """<?xml version="1.0"?>
<TaggedJob xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <EncodingSpeed />
  <Job xsi:type="VideoJob">
    <Input>{{ BASEPATH }}.avs</Input>
    <Output>{{ OUTPUT }}.mp4</Output>
    <FilesToDelete />
    <Zones />
    <DAR>
    </DAR>
    <Settings xsi:type="x264Settings">
      <EncodingMode>9</EncodingMode>
      <BitrateQuantizer>20</BitrateQuantizer>
      <KeyframeInterval>250</KeyframeInterval>
      <NbBframes>3</NbBframes>
      <MinQuantizer>10</MinQuantizer>
      <MaxQuantizer>51</MaxQuantizer>
      <V4MV>false</V4MV>
      <QPel>false</QPel>
      <Trellis>false</Trellis>
      <CreditsQuantizer>40</CreditsQuantizer>
      <Logfile>{{ BASEPATH }}.stats</Logfile>
      <VideoName />
      <CustomEncoderOptions />
      <FourCC>0</FourCC>
      <MaxNumberOfPasses>3</MaxNumberOfPasses>
      <NbThreads>0</NbThreads>
      <x264Preset>99</x264Preset>
      <Turbo>migrated</Turbo>
      <X264Nalhrd>migrated</X264Nalhrd>
      <x264PresetLevel>medium</x264PresetLevel>
      <x264Tuning>0</x264Tuning>
      <QuantizerCRF>20</QuantizerCRF>
      <EncodeInterlaced>false</EncodeInterlaced>
      <NoDCTDecimate>false</NoDCTDecimate>
      <PSNRCalculation>false</PSNRCalculation>
      <NoFastPSkip>false</NoFastPSkip>
      <NoiseReduction>0</NoiseReduction>
      <NoMixedRefs>false</NoMixedRefs>
      <X264Trellis>1</X264Trellis>
      <NbRefFrames>3</NbRefFrames>
      <AlphaDeblock>0</AlphaDeblock>
      <BetaDeblock>0</BetaDeblock>
      <SubPelRefinement>7</SubPelRefinement>
      <MaxQuantDelta>4</MaxQuantDelta>
      <TempQuantBlur>0</TempQuantBlur>
      <BframePredictionMode>1</BframePredictionMode>
      <VBVBufferSize>0</VBVBufferSize>
      <VBVMaxBitrate>0</VBVMaxBitrate>
      <METype>1</METype>
      <MERange>16</MERange>
      <MinGOPSize>25</MinGOPSize>
      <IPFactor>1.4</IPFactor>
      <PBFactor>1.3</PBFactor>
      <ChromaQPOffset>0</ChromaQPOffset>
      <VBVInitialBuffer>0.9</VBVInitialBuffer>
      <BitrateVariance>1.0</BitrateVariance>
      <QuantCompression>0.6</QuantCompression>
      <TempComplexityBlur>20</TempComplexityBlur>
      <TempQuanBlurCC>0.5</TempQuanBlurCC>
      <SCDSensitivity>40</SCDSensitivity>
      <BframeBias>0</BframeBias>
      <PsyRDO>1.0</PsyRDO>
      <PsyTrellis>0</PsyTrellis>
      <Deblock>true</Deblock>
      <Cabac>true</Cabac>
      <UseQPFile>false</UseQPFile>
      <WeightedBPrediction>true</WeightedBPrediction>
      <WeightedPPrediction>2</WeightedPPrediction>
      <NewAdaptiveBFrames>1</NewAdaptiveBFrames>
      <x264BFramePyramid>2</x264BFramePyramid>
      <ChromaME>true</ChromaME>
      <MacroBlockOptions>3</MacroBlockOptions>
      <P8x8mv>true</P8x8mv>
      <B8x8mv>true</B8x8mv>
      <I4x4mv>true</I4x4mv>
      <I8x8mv>true</I8x8mv>
      <P4x4mv>false</P4x4mv>
      <AdaptiveDCT>true</AdaptiveDCT>
      <SSIMCalculation>false</SSIMCalculation>
      <QuantizerMatrix>Flat (none)</QuantizerMatrix>
      <QuantizerMatrixType>0</QuantizerMatrixType>
      <DeadZoneInter>21</DeadZoneInter>
      <DeadZoneIntra>11</DeadZoneIntra>
      <OpenGop>0</OpenGop>
      <X264PullDown>0</X264PullDown>
      <SampleAR>0</SampleAR>
      <ColorMatrix>0</ColorMatrix>
      <ColorPrim>0</ColorPrim>
      <Transfer>0</Transfer>
      <AQmode>1</AQmode>
      <AQstrength>1.0</AQstrength>
      <QPFile>.qpf</QPFile>
      <fullRange>false</fullRange>
      <x264AdvancedSettings>false</x264AdvancedSettings>
      <Lookahead>40</Lookahead>
      <NoMBTree>true</NoMBTree>
      <ThreadInput>true</ThreadInput>
      <NoPsy>false</NoPsy>
      <Scenecut>true</Scenecut>
      <Nalhrd>0</Nalhrd>
      <X264Aud>false</X264Aud>
      <X264SlowFirstpass>false</X264SlowFirstpass>
      <PicStruct>false</PicStruct>
      <FakeInterlaced>false</FakeInterlaced>
      <NonDeterministic>false</NonDeterministic>
      <SlicesNb>0</SlicesNb>
      <MaxSliceSyzeBytes>0</MaxSliceSyzeBytes>
      <MaxSliceSyzeMBs>0</MaxSliceSyzeMBs>
      <Profile>2</Profile>
      <Level>15</Level>
    </Settings>
  </Job>
  <RequiredJobNames />
  <EnabledJobNames />
  <Name>{{ JOB }}</Name>
  <Status>WAITING</Status>
  <Start>0001-01-01T00:00:00</Start>
  <End>0001-01-01T00:00:00</End>
</TaggedJob>
"""


@AsyncCallWrapper
def GetVideoFiles():

	dir = ''
	options = wx.OPEN|wx.FILE_MUST_EXIST|wx.FD_MULTIPLE
	dlg = wx.FileDialog(avsp.GetWindow(), 'Webms', dir, '', '*.avs', options)
	
	filePaths = []
	ID = dlg.ShowModal()
	if ID == wx.ID_OK:
		filePaths = dlg.GetPaths()
	
	dlg.Destroy()
	return filePaths
	
	
@AsyncCallWrapper
def GetOutputName(basename):

	dlg = wx.TextEntryDialog(avsp.GetWindow(), 'Nom de sortie',  defaultValue=basename)
	dlg.ShowModal()
	result = dlg.GetValue()
	dlg.Destroy()
	
	return result


def GenerateJob(file_path, digits, output_name):
	global job_tpl
	global megui_path
	global index
	
	# Find the right index - do not overwrite an existing file
	mp4File = ''
	while True:
		name = output_name + str(index).zfill(digits)
		mp4File = os.path.join( os.path.dirname(file_path), name )
		if not os.path.exists( mp4File + ".mp4" ):
			break
		
		index += 1
	
	# Prepare the content
	job_name = 'job' + str(index)
	job_file_name = job_name + '.xml'
	tpl = job_tpl.replace( "{{ BASEPATH }}", file_path.replace( ".avs", "" ))
	tpl = tpl.replace( "{{ JOB }}", job_name )
	tpl = tpl.replace( "{{ OUTPUT }}", mp4File )
	
	# Write the content
	newFile = open( 
		os.path.join(megui_path, job_file_name),
		'w' )

	newFile.write( tpl )
	newFile.close()
	
	# Upgrade the index for next invocation
	index += 1
	
	return job_name
	
	
def UpdateJobsList(job_names):
	global job_list_tpl
	global megui_path
	
	# Prepare the replacement
	content = ''
	for name in job_names:
		content += '<string>'
		content += name
		content += "</string>\n"

	# Update and write
	tpl = job_list_tpl.replace( "{{ LIST }}", content)
	newFile = open( 
		os.path.join(megui_path, '../joblists.xml'),
		'w' )

	newFile.write( tpl )
	newFile.close()


# Default behavior
index = 1
file_paths = GetVideoFiles()
if file_paths:
	bn = re.sub(r"(?i)\.\w{2,3}_\d+\.avs$", '', os.path.basename(file_paths[0]))
	bn += '_webm_'
	bn = GetOutputName(bn)

	digits = len(str(len(file_paths)))
	job_names = []
	for i, file_path in enumerate(file_paths):
		name = GenerateJob(file_path, digits, bn)
		job_names.append(name)

	UpdateJobsList(job_names)
