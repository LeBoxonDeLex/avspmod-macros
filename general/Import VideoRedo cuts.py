	##########################################################################
# Import VideoRedo cuts
#  -> Recherche le projet VideoRedo .Vprj correspondant � la vid�o .ts du script
#     avisynth en cours pour en importer la d�coupe sous forme de trim(...)
#     /!\ ne fonctionne pour l'instant qu'avec des fps entier !!!
##########################################################################
import sys, locale, os, re

# Jeu de caract�res par d�faut
reload(sys)
sys.setdefaultencoding(locale.getdefaultlocale()[1])

global fps

# findProjectname
# Recherche du fichier projet Vid�oRedo .Vprj
def findProjectname():
	scriptname = avsp.GetScriptFilename()
	projectname = ""

	if scriptname != "":

		# projet ayant la m�me racine que le fichier avisynth (AVSGen)
		indexts = scriptname.rfind(".ts")
		if indexts != -1:
			projectname = scriptname[:indexts] + ".Vprj"
			if os.path.exists(projectname):
				return projectname
		
		indexts = scriptname.rfind(".mp4")
		if indexts != -1:
			projectname = scriptname[:indexts] + ".Vprj"
			if os.path.exists(projectname):
				return projectname
		
		indexts = scriptname.rfind(".mkv")
		if indexts != -1:
			projectname = scriptname[:indexts] + ".Vprj"
			if os.path.exists(projectname):
				return projectname
		
		indexts = scriptname.rfind(".TS")
		if indexts != -1:
			projectname = scriptname[:indexts] + ".Vprj"
			if os.path.exists(projectname):
				return projectname
		
		# projet ayant la m�me racine que le fichier avisynth (bis)
		indexavs = scriptname.rfind(".avs")
		if indexavs != -1:
			projectname = scriptname[:indexavs] + ".Vprj"
			if os.path.exists(projectname):
				return projectname

		# projet ayant la m�me racine que la vid�o dans le script
		indexts = avsp.GetText().find(".ts")
		if indexts != -1:
			indexvid = avsp.GetText().rfind("\"", 0, indexts)
			if indexvid != -1:
				projectname = avsp.GetText()[indexvid+1:indexts].strip() + ".Vprj"

				# si nom de fichier simple: ajout de l'arborescence
				if projectname.find("\\") == -1:
					indexpath = scriptname.rfind("\\")
					projectname = scriptname[:indexpath] + "\\" + projectname
				if os.path.exists(projectname):
					return projectname
		
		# projet ayant la m�me racine que le fichier avisynth (AVSGen) - mais m2ts
		indexts = scriptname.rfind(".m2ts")
		if indexts != -1:
			projectname = scriptname[:indexts] + ".Vprj"
			if os.path.exists(projectname):
				return projectname
		
		# Sinon: parcours sur le disque
		projectname = avsp.GetFilename(title="Projet Videoredo (.Vprj) correspondant au script")
		if len(projectname) > 5:
			if projectname[len(projectname)-5:].lower() == ".vprj":
				return projectname

	return ""
# end def #############################


# getCutList
# R�cup�re les d�coupes d'un projet VideoRedo .Vprj
def getCutList( vprjFileContent, fps ):
	
	ltrimlist = vprjFileContent.split("CutStart=\"")
	rtrimlist = vprjFileContent.split("CutEnd=\"")
	duration = re.search( r'<Duration>(.*)</Duration>', vprjFileContent, re.M | re.I )
	if not duration:
		return '# Oops! Could not find duration in VRD project.'
	
	trimlist = ''
	if (len(ltrimlist) == len(rtrimlist)) and len(ltrimlist) > 1:
		
		# No cut at the beginning?
		if (ltrimlist[1][:11] != "00:00:00;00"):
			frameEnd = convToFrameNb(fps, ltrimlist[1][:11])
			frameEnd -= 1 # Lex: remove 1 frame
			trimlist += "0," + str(frameEnd)
			if len(ltrimlist) > 2:
				trimlist += ";" #s�parateur

		# Middle cuts
		for n in range(2, len(ltrimlist)):
			frameStart = convToFrameNb(fps, rtrimlist[n-1][:11])
			frameEnd = convToFrameNb(fps, ltrimlist[n][:11])
			frameEnd -= 1 # Lex: remove 1 frame
			trimlist += str(frameStart) + "," + str(frameEnd)
			if n < len(ltrimlist) - 1:
				trimlist += ";" #s�parateur

		# No cut at the end?
		lastIndex = len(rtrimlist) - 1
		frameStart = convToFrameNb(fps, rtrimlist[lastIndex][:11])
		
		# Duration contains the time in seconds, followed by 7 digits
		durationInSeconds = duration.group(1)[:-7]
		maxFrameEnd = int(durationInSeconds) * fps
		
		# If there is no significant difference between the values,
		# then there is a scene at the end
		if( maxFrameEnd - frameStart > 30 ):
			if trimlist:
				trimlist += ';'
			
			# VRD's FPS is not always precise, cut 2 frames for safety
			trimlist += str(frameStart) + ',last.frameCount() - 2'
		
		return trimlist

	return ''
# end def #############################


# convToFrameNb
# Convertit une heure en num�ro de frame
def convToFrameNb(fps, h):
	datelist = h.split(":")
	h = int(datelist[0])
	m = int(datelist[1])
	s = int(datelist[2].split(";")[0])
	f = int(datelist[2].split(";")[1])
	return int((h * 3600 + m * 60 + s) * fps + f)
# end def #############################


# findfps
# Recherche le fps dans le script
# (� am�liorer !!!)
def findfps():
	scripttxt = avsp.GetText()
	i=-1
	
	# Recherche d'une occurence de fps
	indexfps = scripttxt.find("fps=")
	if indexfps != -1:
		i = indexfps + 4
	else:
		indexfps = scripttxt.find("fps =")
		if indexfps != -1:
			i = indexfps + 5
	
	# Recherche d'un fps entier ou d�cimal
	if indexfps != -1:
		scripttxt = scripttxt[i:len(scripttxt)].strip()
		j=0
		fpsstring = ""
		while (j<len(scripttxt)) and ((scripttxt[j].isdigit()) or (scripttxt[j] == ".")):
			fpsstring += scripttxt[j]
			j+=1
		fpsstring = fpsstring.strip(".")
		if fpsstring:
			if fpsstring.isdigit():
				return int(fpsstring)
			else:
				return float(fpsstring)
	return -1
# end def #############################

# Lex
def buildTrimList( vprjFileContent, offset="0" ):
	
	global fps
	fps = findfps()
	if fps == -1:
		fps = avsp.GetVideoFramerate()
	if float(fps) == int(fps):
		fps = int(fps)

	cutlist = getCutList( vprjFileContent, fps )
	trimList = ""
	if cutlist:
		trimList += "# Trim d'ajustement\n" + "offset=" + offset
		trimList += "\nTrim(offset, last.framecount)\n\n"
		trimList += "# Trim import� depuis videoRedo\n"
		trimList += "Trim(" + (") ++ Trim(").join(cutlist.split(";")) + ")\n"
	
	return trimList
# Lex


# Main
v = findProjectname()
if v == "":
	avsp.MsgBox("Fichier vid�o non d�fini")
else:
	f = open(v,'r').read()
	trimList = buildTrimList( f )
		
	if trimList:
		newscripttxt = avsp.GetText() + "\n" + trimList
		avsp.SetText(newscripttxt)
		#avsp.MsgBox("Pensez � corriger l'offset !!!")
	else:
		avsp.MsgBox("Auune d�coupe VideoRedo trouv�e")
