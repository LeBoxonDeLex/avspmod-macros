##########################################################################
# Moves the current frame to the next cut end.
# It is useful to verify cut points, in particular with Trims imported from VideoRedo.
##########################################################################

import sys, locale


# Default charset
reload(sys)
sys.setdefaultencoding(locale.getdefaultlocale()[1])

##########################################
# GoToNextTrim
# Finds the next "current fame"
##########################################
def GoToNextTrim(s):
	
	splittrim = s.split('++')
	if len(splittrim) < 1:
		return -1
	
	nextFrame = 0
	for i in splittrim:
		i2 = str(i.lower().strip())

		# Check the Trim syntax
		if i2.startswith('trim(') and i2.endswith(')'):
			strtrimlist = i2[5:len(i2)-1].split(',')
			if len(strtrimlist) == 2:
				frame1 = strtrimlist[0].strip()
				frame2 = strtrimlist[1].strip()
				if (not frame1.isdigit()) or (not frame2.isdigit()):
					return -2
				
				trimLength = (int(frame2)) - (int(frame1)) + 1
				nextFrame += trimLength
				if nextFrame > avsp.GetFrameNumber():
					break
	# end for
	return nextFrame
# end def #############################

# Search for Trims
scriptlines = avsp.GetText().splitlines()
lastTrim = ""
for l in scriptlines:
	ll = l.lower().strip()
	if ll.startswith('trim('):
		lastTrim = l;

if len( lastTrim ) > 0:
	nextFrame = GoToNextTrim( lastTrim )
	if nextFrame > 0:
		avsp.ShowVideoFrame( nextFrame )
