##########################################################################
# Removes the last frame of every Trim.
#    Example: Trim(2085, 2310) ++ Trim(2749, 3016)
#    ... will be replaced by: Trim(2085,2309) ++ Trim(2749,3015)
# Lines with non-numeric chartacters are ignored.
##########################################################################

import sys, locale


# Default charset
reload(sys)
sys.setdefaultencoding(locale.getdefaultlocale()[1])

##########################################
# updateTrim
##########################################
def updateTrim(s):
	
	splittrim = s.split('++')
	if len(splittrim) < 1:
		return ""

	newcmd = "";
	for i in splittrim:
		i2 = str(i.lower().strip())

		# Check the Trim syntax
		if i2.startswith('trim(') and i2.endswith(')'):
			strtrimlist = i2[5:len(i2)-1].split(',')
			if len(strtrimlist) == 2:
				frame1 = strtrimlist[0].strip()
				frame2 = strtrimlist[1].strip()
				if (not frame1.isdigit()) or (not frame2.isdigit()):
					return ""
				frame1 = int(frame1) +1
				frame2 = int(frame2)

		# Generate a new Trim
		if len(newcmd) > 0:
			newcmd = newcmd + ' ++ Trim(' + str(frame1) + ',' + str(frame2) + ')'
		else:
			newcmd = 'Trim(' + str(frame1) + ',' + str(frame2) + ')'
	#end for

	return newcmd
# end def #############################

# Search for Trims
scriptlines = avsp.GetText().splitlines()
newscripttxt = ''
for l in scriptlines:
	if l.lower().strip().startswith('trim('):
		newline = updateTrim(l)
		if newline:
			newscripttxt += '#' + l + '\n' + newline + '\n'
		else:
			newscripttxt += l + '\n'
	else:
		newscripttxt += l + '\n'
avsp.SetText(newscripttxt)
