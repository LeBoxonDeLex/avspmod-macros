# Selects VideoRedo project files and generates AVS
# files from a given skeleton.

import os
import sys
import wx
import re
from avsp import AsyncCallWrapper

#
# Use avsp.SetText( msg ) to debug.
#

# Global "preference"
def AVSP_MOD_LOC():
	return 'I:/Outils/TraitementHD/AVSGen/templates/'

# Find Vprj files
@AsyncCallWrapper
def getVprjFiles():

	dir = '' #self.options['recentdir']
	options = wx.OPEN|wx.FILE_MUST_EXIST|wx.FD_MULTIPLE
	dlg = wx.FileDialog(avsp.GetWindow(), 'VideoRedo Projects', dir, '', '*.VPrj', options)
	
	ID = dlg.ShowModal()
	if ID == wx.ID_OK:
		filenames = dlg.GetPaths()
	else:
		filenames = []
	dlg.Destroy()
	return filenames


# Get the FPS
@AsyncCallWrapper
def getFps():
	
	fps = '25'
	options = wx.OK | wx.CANCEL
	dlg = wx.TextEntryDialog( avsp.GetWindow(), 'FPS', 'Quel FPS utiliser ?', fps, options )
	
	ID = dlg.ShowModal()
	if ID == wx.ID_OK:
		fps = dlg.GetValue()
	
	dlg.Destroy()
	return fps


# Generate AVS files
def generateAvsFiles():
	
	# The result is an array of generated AVS files
	generated = []
	
	# Which fps to use?
	fps = str( getFps())
	
	# Read the template
	# FIXME: use try/catch
	avsTplFile = open( AVSP_MOD_LOC() + 'Webm.avs', 'r' )
	avsTpl = avsTplFile.read()
	avsTplFile.close()
	
	# Instantiate it for every VPrj file
	for f in getVprjFiles():
		projFile = open( f, 'r' )
		projFileContent = projFile.read()
		projFile.close()
		
		err = re.search( r'<Filename>(.*)</Filename>', projFileContent, re.M | re.I )
		if not err:
			avsp.SetText( 'Oh! No!' )
			return generated
		
		contentToWrite = avsTpl
		contentToWrite = contentToWrite.replace( '__PLUGINSPATH__', AVSP_MOD_LOC() + 'plugins' )
		contentToWrite = contentToWrite.replace( '25', fps )
		contentToWrite = contentToWrite.replace( '__VID__', err.group( 1 ))
		
		pattern = re.compile( '\.vprj', re.IGNORECASE )
		newFileName = pattern.sub( '.avs', f )
		newFile = open( newFileName, 'w' );
		newFile.write( contentToWrite )
		newFile.close()
		
		generated.append( newFileName )

	return generated

# Default behavior
generated = generateAvsFiles()

# Anything to do next?
for path in generated:
	avsp.OpenFile( filename=path )
	#avsp.ShowVideoFrame()
	
	avsp.ExecuteMenuCommand( 'Macros -> Import VideoRedo cuts' )
	avsp.ExecuteMenuCommand( 'Macros -> Remove 1 Frame per Trim' )
	#avsp.ExecuteMenuCommand( 'F5' )
	avsp.ExecuteMenuCommand( 'Ctrl+S' )
